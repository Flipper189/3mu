package de.flipper189.editor;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.BorderLayout;

import java.io.File;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.ListSelectionModel;


public class Gui extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Playlist data;
	private JPanel panelButton, panelList;
	private JMenuBar menuBar;
	private JMenu mnDatei;
	private JMenuItem mntmNeu, mntmffnen, mntmSpeichern, mntmSpeichernUnter;
	private JList<String> list;
	private JButton button_add;
	private JButton button_remove;
	private JButton button_up;
	private JButton button_down;
	private JScrollPane scrollPane;
	private JButton button_play;
	private JMenuItem mntmBeenden;
	private File lastPlaylist;
	private File lastFile;
	private JMenu mnHilfe;
	private JMenuItem mntmber;
	private JMenuItem mntmAbspielen;


	/**
	 * Create the application.
	 */
	public Gui(Playlist playlist) {
		
		data = playlist;
		
		//Window
		//frame = new JFrame();
		this.setBounds(100, 100, 600, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("3Mu - Playlisteditor");
		
		//Panel
		panelButton = new JPanel();
		getContentPane().add(panelButton, BorderLayout.SOUTH);

		
		panelList = new JPanel();
		getContentPane().add(panelList, BorderLayout.CENTER);
		
		//Menu
		menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		
		mnDatei = new JMenu("Datei");
		menuBar.add(mnDatei);
		
		mntmNeu = new JMenuItem("Neu");
		mntmNeu.addActionListener(e-> newfile());
		mnDatei.add(mntmNeu);
		
		mntmffnen = new JMenuItem("Öffnen");
		mntmffnen.addActionListener(e-> openfile());
		mnDatei.add(mntmffnen);
		
		mntmSpeichern = new JMenuItem("Speichern");
		mntmSpeichern.addActionListener(e-> savefile());
		mnDatei.add(mntmSpeichern);
		
		mntmSpeichernUnter = new JMenuItem("Speichern unter");
		mntmSpeichernUnter.addActionListener(e-> savefileas());
		mnDatei.add(mntmSpeichernUnter);
		
		mntmAbspielen = new JMenuItem("Abspielen");
		mntmAbspielen.addActionListener(e-> data.playPlaylist());
		mnDatei.add(mntmAbspielen);
		
		mntmBeenden = new JMenuItem("Beenden");
		mntmBeenden.addActionListener(e-> System.exit(0));
		mnDatei.add(mntmBeenden);
		
		mnHilfe = new JMenu("Hilfe");
		menuBar.add(mnHilfe);
		
		mntmber = new JMenuItem("Über");
		mntmber.addActionListener(e-> about());
		mnHilfe.add(mntmber);
		
		//Buttons
		button_up = new JButton("∧");
		button_up.addActionListener(e-> itemup());
		panelButton.add(button_up);
		
		button_down = new JButton("∨");
		button_down.addActionListener(e-> itemdown());
		panelButton.add(button_down);
		
		button_remove = new JButton("-");
		button_remove.addActionListener(e-> removeitem());
		panelButton.add(button_remove);
		
		button_add = new JButton("+");
		button_add.addActionListener(e-> additem());
		panelButton.add(button_add);
		
		button_play = new JButton(">");
		button_play.addActionListener(e-> itemplay());
		panelButton.add(button_play);
		panelList.setLayout(new BorderLayout(0, 0));	
		
		
		//Liste
		list = new JList(data.getList().toArray());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setTransferHandler(new FileDropHandler(this)); //Drag and Drop
		//panelList.add(list, BorderLayout.CENTER);
		scrollPane = new JScrollPane(list);
		panelList.add(scrollPane);
		
		this.setLocationRelativeTo(null); //Positionert das Fenter mittig im Bildschirm
		
		
		reload();
	}
	
	
	private void openfile() {
		//Quelle: https://www.java-tutorial.org/jfilechooser.html
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(lastPlaylist);
        int chooseroption = chooser.showOpenDialog(null);
        if(chooseroption == JFileChooser.APPROVE_OPTION)
        {
        	data.openPlaylist(chooser.getSelectedFile().toString());
        	lastPlaylist = chooser.getSelectedFile();
        }
        reload();
	}
	
	
	private void newfile() {
		data.newPlaylist();
		reload();
	}
	
	
	private void savefile() {
		if(data.fileIsSaved()) {
			data.savePlaylist();
		}
		else {
			savefileas();
		}
		
	}
	
	
	private void savefileas() {
		//Quelle: https://www.java-tutorial.org/jfilechooser.html
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(lastPlaylist);
        int chooseroption = chooser.showSaveDialog(null);
        if(chooseroption == JFileChooser.APPROVE_OPTION)
        {
        	data.saveAsPlaylist(chooser.getSelectedFile().toString());
        	lastPlaylist = chooser.getSelectedFile();
        }
	}
	
	
	
	private void additem() {
		//Quelle: https://www.java-tutorial.org/jfilechooser.html
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(true);
        chooser.setCurrentDirectory(lastFile);
        int chooseroption = chooser.showOpenDialog(null);
        if(chooseroption == JFileChooser.APPROVE_OPTION)
        {
        	//data.addFile(chooser.getSelectedFile().toString());
        	File file[] = chooser.getSelectedFiles();
        	for(File f: file) {
        		data.entityAdd(f.toString());
        	}
        	lastFile = chooser.getSelectedFile();
        }
        reload();
	}
	
	
	public void addDragAndDropItem(List<File> files) {
    	for(File f: files) {
    		data.entityAdd(f.toString());
    	}
    	reload();
	}
	
	
	private void removeitem() {
		int item = list.getSelectedIndex();
		data.entityRemove(item);
		reload();
		if(item < data.getList().size()) {
			list.setSelectedIndex(item);
		}
		else {
			list.setSelectedIndex(data.getList().size()-1);
		}
	}
	
	
	private void itemup() {
		int index = list.getSelectedIndex();
		data.entityUp(index);
		reload();
		if(index > 0) {
			list.setSelectedIndex(index-1);
		}
	}
	
	private void itemdown() {
		int index = list.getSelectedIndex();
		data.entityDown(index);
		reload();
		if(index > 0) {
			list.setSelectedIndex(index+1);
		}
	}
	
	
	private void itemplay() {
		data.entityPlay(list.getSelectedIndex());
	}
	
	
	private void about() {
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame,"3Mu\nVersion 0.5\nFlipper189, 2022");
	}
	
	
	
	private void reload() {
		DefaultListModel listModel = new DefaultListModel();
		for (int i = 0; i < data.getList().size(); i++)
		{
		    listModel.addElement(data.getList().get(i));
		}
		list.setModel(listModel);
	}
	
	

}
