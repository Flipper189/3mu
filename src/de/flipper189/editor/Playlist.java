package de.flipper189.editor;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Speichert die Playlist + IO Operationen
 * @author sebastian
 *
 */
public class Playlist {

	private String playlistPath;
	private String playlistName;
	private boolean isSaved;
	private ArrayList<String> list;
	
	
	/**
	 * Konstruktor
	 */
	public Playlist() {
		playlistPath = "";
		playlistName = "";
		isSaved = false;
		list = new ArrayList<String>();
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<String> getList() {
		return list;
	}
	
	
	public boolean fileIsSaved() {
		return isSaved;
	}
	
	
	/**
	 * Gibt die Playlist in der Konsole aus
	 * Nur für Testzwecke
	 */
	public void printList() {
		System.out.println("-------------------------");
		for(String s: list) {
			System.out.println(s);
		}
	}
	
	
	
	/**
	 * Fügt einen neuen Eintrag der Playlist hinzu wenn dieser noch nicht enthalten ist
	 * @param Name der Datei
	 */
	public void entityAdd(String name) {
		if(checkFile(name) && !list.contains(name)) {
			list.add(name);
		}
	}
	
	
	/**
	 * Löscht einen Eintrag aus der Playlist
	 * @param index des Eintrags
	 */
	public void entityRemove(int index) {
		if(index >= 0 && index <= list.size()) {
			list.remove(index);
		}
	}
	
	
	/**
	 * Verschiebt einen Eintrag in der Playliste um einen nach Oben
	 * @param index  des Eintrags
	 */
	public void entityUp(int index) {
		if(index > 0) {
			String temp = list.get(index-1);
			list.set(index-1, list.get(index));
			list.set(index, temp);
		}
	}
	
	
	/**
	 * Verschiebt einen Eintrag in der Playliste um einen nach Unten
	 * @param index des Eintrags
	 */
	public void entityDown(int index) {
		if (index < list.size()-1) {
			String temp = list.get(index+1);
			list.set(index+1, list.get(index));
			list.set(index, temp);
		}
	}
	
	
	/**
	 * Spielt einen Eintrag in der Playliste mit dem Standartplayer des Betriebssystems ab
	 * @param index
	 */
	public void entityPlay(int index) {
		//https://wiki.byte-welt.net/wiki/Dokument_mit_Standardanwendung_%C3%B6ffnen_(Java)
		if(index >= 0 && index < list.size()) {
			if(Desktop.isDesktopSupported()) {
				   Desktop desk = Desktop.getDesktop();
				   try {
					desk.open(new File(list.get(index)));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
	}
	
	
	/**
	 * Spiele die gesamte Playlist mit den Standartplayer des Betriebssystems ab
	 */
	public void playPlaylist() {
		if(isSaved) {
			if(Desktop.isDesktopSupported()) {
				   Desktop desk = Desktop.getDesktop();
				   try {
					desk.open(new File(playlistPath+"/"+playlistName));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	/**
	 * Öffnent eine Playlist im Editor
	 * Dabei werden die Realativen Plade in der Playlistdatein in absolute umgewandert und je nach OS die Trennzeichen der Pfade angepasst 
	 * @param file
	 */
	public void openPlaylist(String file) {
		File playlist = new File(file);
		if(playlist.exists() && playlist.canRead()) {
			//System.out.println(playlist.getName());
			//System.out.println(playlist.getParent());
			newPlaylist();
			playlistPath = playlist.getParent();
			playlistName = playlist.getName();
			isSaved = true;
			
			//Read File
			InputStream istream;
			try {
				istream = new FileInputStream(playlist);
				Scanner reader = new Scanner(istream);
				String line = "";
				while(reader.hasNext()) {
					line = reader.nextLine();
					if(line.charAt(0) != '#') {
						
						String os = System.getProperty("os.name").toLowerCase();
						if (!os.contains("win")){
						    line = line.replace('\\','/');
						}
						else {
							line = line.replace('/','\\');
						}

						line = pathToAbsolute(line);
						entityAdd(line);
					}
				}
				reader.close();
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		//printList();
	}
	
	
	/**
	 * 
	 */
	public void savePlaylist() {
		if(isSaved) {
			saveList(playlistPath + "/" + playlistName);
		}
	}
	
	
	/**
	 * 
	 * @param name
	 */
	public void saveAsPlaylist(String name) {
		System.out.println("Dateiname " + name);
		if(!name.contains(".m3u")) {
			name = name + ".m3u";
		}
		File playlist = new File(name);
		playlistPath = playlist.getParent();
		playlistName = playlist.getName();
		isSaved = true;
		saveList(playlist.getPath());
	}
	
	
	/**
	 * erstellt eine neue Playlist
	 * Alle Eigenschaften der Playlist werden zurückgesetzt
	 */
	public void newPlaylist() {
		playlistPath = "";
		playlistName = "";
		isSaved = false;
		list.removeAll(list);
	}
	
	
	/**
	 * Prüft ab die Datei existiert
	 * @param name
	 * @return
	 */
	private boolean checkFile(String name) {
		File file = new File(name);
		return file.exists();
	}
	
	
	/**
	 * wandelt absolute Dateipade in realative um, ausgehen vom Speicherort der Playlist Datei
	 * @param name
	 * @return
	 */
	private String pathToRelativ(String name) {
		Path playlist = Paths.get(playlistPath);
		Path file = Paths.get(name);
		return playlist.relativize(file).toString();
	}
	
	
	/**
	 * wandelt relative Dateipade in absolute um, ausgehen vom Speicherort der Playlist Datei
	 * @param name
	 * @return
	 */
	private String pathToAbsolute(String name) {
		Path file = Paths.get(playlistPath + "/" + name);
		return file.normalize().toString();
	}
	
	
	/**
	 * Speichert die Playlist in eine Datei
	 * @param name der Datei
	 */
	private void saveList(String name) {
		File playlist = new File(name);
		System.out.println("Schreibe Datei");
		OutputStream ostream;
		try {
			ostream = new FileOutputStream(playlist);
			PrintWriter writer = new PrintWriter(ostream);
			writer.println("#Playlist");
			for(String s: list) {
				String line = pathToRelativ(s);
				writer.println(line);
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
}
