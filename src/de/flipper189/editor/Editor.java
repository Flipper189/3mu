package de.flipper189.editor;

public class Editor {

	private Playlist playlist;
	private Gui gui;
	
	
	public Editor() {
		playlist = new Playlist();
		gui = new Gui(playlist);
	}
	
	
	public static void main(String[] args) {

		Editor editor = new Editor();
		editor.gui.setVisible(true);
	}

}
