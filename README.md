# 3Mu - Playlisteditor



## Beschreibung
3Mu ist eine Editor für m3u Playlisten.


## Funktionen
- erstellen und bearbeiten von Playlisten
- Playlisten und einzelne Titel abspielen
- unterstützt Drag & Drop aus dem Dateiexplorer
- unterstützt Windows und Unix Pfade

## Systemandorderungen
- Java 8 oder höher

## Download
[neuste Version](https://gitlab.com/Flipper189/3mu/-/blob/main/build/3Mu.jar)

## Lizens
[GNU GPLv3](https://gitlab.com/Flipper189/3mu/-/blob/main/LICENSE)